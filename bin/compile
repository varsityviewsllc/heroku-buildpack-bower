#!/usr/bin/env bash

####### Configure environment

set -o errexit    # always exit on error
set -o errtrace   # trap errors in functions as well
set -o pipefail   # don't ignore exit codes when piping output
set -o posix      # more strict failures in subshells
# set -x          # enable debugging

# Configure directories
build_dir=$1
cache_dir=$2
env_dir=$3
bp_dir=$(cd $(dirname $0); cd ..; pwd)
heroku_dir=$build_dir/.heroku
warnings=$(mktemp)

# Load dependencies
source $bp_dir/lib/common.sh
source $bp_dir/lib/build.sh
source $bp_dir/lib/warnings.sh

# Avoid GIT_DIR leak from previous build steps
unset GIT_DIR

# Provide hook to deal with errors
trap build_failed ERR

####### Determine current state

head "Reading application state"
read_current_state
show_current_state

warn_bower_components "$components_source"

####### Build the project's dependencies

head "Building dependencies"
cd $build_dir
build_dependencies

####### Finalize the build

head "Finalizing build"
clean_bower
clean_cache
create_cache
build_succeeded
