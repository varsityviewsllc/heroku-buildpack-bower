warn_bower_components() {
  local components_source=$1
  if [ "$components_source" == "" ]; then
    warning "No bower.json found"
  fi
}
